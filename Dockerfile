FROM ubuntu:xenial

MAINTAINER Bryan Woods <bryan.k.woods@gmail.com>

# it is very important to chain the update and install steps
RUN apt-get update && apt-get install -y \
    python \
    python-pip \
    ipython \
    ipython-notebook \
    python-numpy \
    python-scipy \
    python-pandas \
    python-matplotlib \
    python-shapely \
    python-mpltoolkits.basemap \
    python-pyproj \
    python-requests \
    python-sklearn \
    libgrib-api-dev \
    libspatialindex-dev \
    python-fiona \
    gdal-bin \
    git \
    vim

# enable a virtual environment
RUN pip install virtualenv
RUN /usr/local/bin/virtualenv /opt/env --distribute --system-site-packages

RUN /opt/env/bin/pip install \
    retrying \
    pygrib \
    jupyter \
    git+git://github.com/geopandas/geopandas.git \
    https://pypi.python.org/packages/b0/6c/6cc8d738f14d5efa0c38ec29403bbd9c75e64b3fe84b53290178dda0dbd9/Rtree-0.8.3.tar.gz

# Install XGBoost
RUN git clone --recursive https://github.com/dmlc/xgboost && \
    cd xgboost && \
    make -j4 && \
    cd python-package; python setup.py install

# setup a local user inside the container
RUN useradd --create-home --home-dir /home/myuser --shell /bin/bash myuser
RUN chown -R myuser /opt/env
RUN adduser myuser sudo

ADD .bashrc.template /home/myuser/.bashrc

ADD scripts/run_ipython.sh /home/myuser/run_ipython.sh
RUN chmod +x /home/myuser/run_ipython.sh
RUN chown myuser /home/myuser/run_ipython.sh

# open a port of Jupyter
EXPOSE 8888

RUN usermod -a -G sudo myuser
RUN echo "myuser ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
USER myuser
RUN mkdir -p /home/myuser/notebooks
ENV HOME=/home/myuser
ENV SHELL=/bin/bash
ENV USER=myuser
VOLUME /home/myuser/notebooks
WORKDIR /home/myuser/notebooks

# add the scripts to the container
COPY scripts/raster2poly.py /home/myuser/raster2poly.py
COPY scripts/predict_values.py /home/myuser/predict_values.py
COPY scripts/geocode_homes.py /home/myuser/geocode_homes.py

# add the needed data to the container
COPY data /home/data

