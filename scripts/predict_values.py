#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')
import pandas as pd
import geopandas as gpd
import numpy as np
import logging
import xgboost as xgb
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from matplotlib import pyplot as plt
from shapely.geometry import Point
import os

logger = logging.getLogger('predict_values')

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='Geocode homes from assessment data.')
    parser.add_argument('--outfile', type=str, default='predicted_assessments.csv')
    parser.add_argument('--plotdir', type=str, default='/home/myuser/notebooks',
                        help="directory to save output")
    parser.add_argument('--assessments', type=str, default='data/just_property_details.csv',
                        help='Name of the input assessments file. Default: %(default)s')
    parser.add_argument('--elevation', type=str, default='data/contours.zip',
                        help='Path to elevation contour shapefile')
    parser.add_argument('--log', dest='logfile', type=str,
                        help="Optional log gile",
                        default=None)
    parser.add_argument('-v', dest='verbose', action='count',
                        help='Log verbosity level', default=0)
    args = parser.parse_args()

    loglevs = {0: logging.ERROR,
               1: logging.WARNING,
               2: logging.INFO,
               3: logging.DEBUG}
    # Clip args.verbose at 3
    if args.verbose > 3:
        args.verbose = 3

    log_level = loglevs[args.verbose]

    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='a',
                            format="%(message)s", level=log_level)
    else:
        logging.basicConfig(format="%(message)s", level=log_level)

    # read the geocoded assessments table
    # df = pd.read_csv('data/just_property_details.csv')
    df = pd.read_csv(args.assessments)
    # build shapely geometries from lat, lon columns
    crs = {'init': 'epsg:4326'}  # define the coordinate system
    geometry = [Point(xy) for xy in zip(df.longitude, df.latitude)]
    geo_df = gpd.GeoDataFrame(df, crs=crs, geometry=geometry)
    # calculate an elevation column -- start by creating a point geometry column
    elev_file = args.elevation
    # use a virtual file system if the shapefile is zipped
    if elev_file.endswith('.zip'):
        # assume shapefile name inside is the same pattern as the zipfile
        shp_name = os.path.splitext(os.path.basename(elev_file))[0]+'.shp'
        # elev_df = gpd.read_file('/contours.shp', vfs='zip:///home/data/contours.zip')
        elev_df = gpd.read_file('/'+shp_name, vfs='zip://'+os.path.abspath(elev_file))
    else:
        elev_df = gpd.read_file(elev_file)

    # set the CRS of the elevation if empty
    if not len(elev_df.crs):
        elev_df.crs = geo_df.crs

    # perform a spatial join -- due to limits with geopandas make sure the polygon is the first argument
    # overwrite the original DataFrame -- note order is no longer preserved
    df = gpd.sjoin(elev_df, geo_df, how='right')

    # drop the no longer needed columns
    del df['index_left'], df['geometry']
    # rename height to elevation
    df = df.rename(columns={'height': 'elevation'})
    
    # cleanse data formatting
    # remove letters from street number
    df['ST#'] =  [int(filter(lambda x: x.isdigit(), s)) for s in df['ST#']]
    # make the zoning categorical
    zone_key = 'ZN'
    label_encoder = LabelEncoder()
    feature = label_encoder.fit_transform(df[zone_key])
    labels = np.unique(feature)
    feature = feature.reshape(len(df), 1)
    # transform to one-hot matrix
    onehot_encoder = OneHotEncoder(sparse=False)
    feature = onehot_encoder.fit_transform(feature)
    # feed the zone OneHot matrix back into new columns
    for i, lab in zip(labels, label_encoder.inverse_transform(labels)):
        new_column = '_'.join([zone_key, str(lab)])
        df[new_column] = feature[:, i].astype(bool)
    
    # do a test/train split
    msk = np.random.rand(len(df)) < 0.8
    train = df[msk]
    test = df[~msk]

    # extract the target variable -- total property value
    target_key = 'TOT'
    drop_keys = {target_key, 'STREET', zone_key}
    feature_keys = list(set(df.keys()) - drop_keys)
    
    dtrain = xgb.DMatrix(train[feature_keys], label=train[target_key])
    dtest = xgb.DMatrix(test[feature_keys], label=test[target_key])

    # train the model -- pesos to dollars says it's a Poisson distribution
    params= {'objective' : 'count:poisson',
             'eval_metric' : 'mae',
             'colsample_bytree' : 0.7,
             'eta' : 0.05,
             'max_depth' : 5,
             'num_round' : 2500}
    gbm = xgb.train(params, dtrain, num_boost_round=2500, evals=[(dtest, 'eval')], early_stopping_rounds=20)
    # you could use this model to make predictions
    # predictions = gbm.predict(dtest)
    xgb.plot_importance(gbm)
    plt.savefig(os.path.join(args.plotdir, 'feature_importance.pdf'), bbox_inches='tight')
