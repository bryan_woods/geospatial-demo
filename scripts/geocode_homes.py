#!/usr/bin/env python

import pandas as pd
import numpy as np
import logging
import itertools
import requests
from retrying import retry


logger = logging.getLogger('geocoder')

# set authentication information
__APP_CODE = 'AJKnXv84fjrb0KIHawS0Tg'
__APP_ID = 'DemoAppId01082013GAL'

_SEARCH_URL = 'https://places.demo.api.here.com/places/v1/discover/search'

@retry(stop_max_attempt_number=5, wait_random_min=1000, wait_random_max=2000)
def find_restaurant(lat, lon, url=_SEARCH_URL):
    """
    Find the distance to the nearest restaurant given coordinate
    
    :param latitude: latitude of location
    :param longitude: longitude of location
    :return: distance in meters to the nearest restaurant
    """
    params = {'q': 'restaurant',
              'at': '%s,%s' % (lat,lon)}
    response = requests.get(url, params=params, headers={'app_id': __APP_ID,
                                                         'app_code': __APP_CODE})
    response_json = response.json()
    logger.debug("API Response: %s", response_json)
    distances = [item['distance'] for item in response_json['results']['items']]
    return min(distances)

@retry(stop_max_attempt_number=5, wait_random_min=1000, wait_random_max=2000)
def query_address(query, url=_SEARCH_URL, coords=(43.00, -71.45)):
    """
    Make a query to the HERE search API
    :param coords: (lat, lon) of query origin
    :param url: URL of the HERE maps API
    :param q: query string
    :return: lat, lon of the top result
    """
    params = {'q': query,
              'at': '%s,%s' % coords}

    response = requests.get(url, params=params, headers={'app_id': __APP_ID,
                                                         'app_code': __APP_CODE})
    response_json = response.json()
    logger.debug("API Response: %s", response_json)
    items = response_json['results']['items']
    return items[0]['position']


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='Geocode homes from assessment data.')
    parser.add_argument('--outfile', type=str, default='geocode_assessments.csv')
    parser.add_argument('--assessments', type=str, default='data/Conway_assessments.xlsx',
                        help='Name of the input assessments file. Default: %(default)s')
    parser.add_argument('--log', dest='logfile', type=str,
                        help="Optional log gile",
                        default=None)
    parser.add_argument('-v', dest='verbose', action='count',
                        help='Log verbosity level', default=0)
    args = parser.parse_args()

    loglevs = {0: logging.ERROR,
               1: logging.WARNING,
               2: logging.INFO,
               3: logging.DEBUG}
    # Clip args.verbose at 3
    if args.verbose > 3:
        args.verbose = 3

    log_level = loglevs[args.verbose]

    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='a',
                            format="%(message)s", level=log_level)
    else:
        logging.basicConfig(format="%(message)s", level=log_level)

    # read the assessments table
    df = pd.read_excel(args.assessments)
    # select just properties with known street addresses (non-zero house numbers)
    df = df[(df['ST#'] != 0) & (df['ST#'].notnull())].copy()
    # build an address string for the search app
    address_string = ['%s %s, %s' % (n, s, c) for (n, s, c) in zip(df['ST#'], df['STREET'],
                                                                   itertools.repeat('Conway, NH'))]

    coord_list = []
    for i, s in enumerate(address_string):
        try:
            coords = query_address(s)
            logging.info("%i of %i: %s at %s", i, len(address_string), s, coords)
            coord_list.append(coords)
        except Exception as err:
            logging.warn("Query exception %s", err)
            coord_list.append((np.nan, np.nan))

    df['latitude'], df['longitude'] = zip(*coord_list)

    # find the nearest restaurants for each property
    distances = []
    for i, (lat, lon) in enumerate(zip(df['latitude'], df['longitude'])):
        try:
            logging.info("Find restaurants for property %i of %i", i, len(df))
            distances.append(find_restaurant(lat, lon))
        except Exception as err:
            logging.warn("Query exception %s", err)
            distances.append(np.nan)
    df['restaurant_distance'] = distances
    df.to_csv(args.outfile)
