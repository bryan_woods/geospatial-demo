#!/usr/bin/env python

from setuptools import setup

setup(
    name='geomanchester',
    version=0.1,
    author='Bryan K Woods',
    author_email='bryan.k.woods@gmail.com',
    description='Demo of geospatial packages for python',
    packages=['geomanchester'],
    package_dir={'': 'lib'},
    scripts=['scripts/geocode_homes.py'],
    # Bypass zipping on build/install process to avoid
    # zip-cache bugs on NFS filesystems.
    zip_safe=False,
    package_data={'geomanchester': ['data/Conway_assessments.xlsx']},
    install_requires=['pandas', 'xlrd', 'pillow'],
)
