This is a talk given to the NH Data Science Meetup on April 20, 2017.

There are demo scripts and Jupyter notebooks showing the results. A Dockerfile is included to deploy the needed environment.

To run the Jupyter notebooks:
docker build -t <container_name> .
docker run -p 8888:8888 -v <local_output_dir>:/home/myuser/notebooks --rm -it <container_name>
~/run_ipython.sh

Follow the instructions from Jupyter to open the notebook in your browser. You will need to copy the link printed to screen which contains your authentication token.